import { PhysicalItemMythras } from '@item/physical'

export class WeaponMythras extends PhysicalItemMythras {
  get damageRoll() {
    const data: any = this.data.data
    if (this.damageModifier) {
      return data.damage + '+' + this.actor.damageMod
    } else {
      return data.damage
    }
  }

  get damageModifier() {
    return (this.data.data as any).damageModifier
  }

  get combatEffects() {
    return (this.data.data as any)['combat-effects']
  }
}
